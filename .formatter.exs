#!/usr/bin/env elixir
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8 tabstop=2:

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Whithat Project Group
# Licensed under BSD-2-Caluse
# File: .formatter.exs (whithat-project/whithat.ex/.formatter.exs)
# Content:
# Copyright (c) 2022 Whithat Project Group All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

# Used by "mix format"
[
	inputs: ["{mix,.formatter}.exs", "{config,src,lib,test}/**/*.{ex,exs}"]
]
