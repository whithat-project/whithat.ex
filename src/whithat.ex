#!/usr/bin/env elixir
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8 tabstop=2:

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Whithat Project Group
# Licensed under BSD-2-Caluse
# File: whithat.ex (whithat-project/whithat.ex/src/whithat.ex)
# Content:
# Copyright (c) 2023 Whithat Project Group All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Whithat do
	@moduledoc """
	Documentation for `Whithat`.
	"""

	@doc """
	Hello world.

	## Examples

			iex> Whithat.hello()
			:world
	"""
	@spec hello :: :world
	def hello, do: :world
	
end
