#!/usr/bin/env elixir
# encoding: utf-8
# -*- coding: utf-8 -*-
# vim:set fileencoding=utf-8 tabstop=2:

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File is From Whithat Project Group
# Licensed under BSD-2-Caluse
# File: whithat_test.exs (whithat-project/whithat.ex/test/whithat_test.exs)
# Content:
# Copyright (c) 2022 Whithat Project Group All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule WhithatTest do
	use ExUnit.Case
	doctest Whithat

	test "greets the world" do
		assert Whithat.hello() == :world
	end
end
